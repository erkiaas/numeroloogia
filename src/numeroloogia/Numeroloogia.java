package numeroloogia;
/**
 * G.McCants'i numeroloogia arvude kalkulaator.
 * @author eaas
 * @version 0.3
 *
 */
public class Numeroloogia {

    public static void main(String[] args) {

    }

    public static int[] LeiaNumbrid (String nimi, int p2ev, int kuu, int aasta) {

        int nimenumbrid[] = PytSys(nimi);
        int synnip2ev = p2ev + kuu + aasta;

        int[] Vastus = new int[6];
        Vastus[0] = Ristsumma(nimenumbrid[0]);
        Vastus[1] = Ristsumma(nimenumbrid[1]);
        Vastus[2] = Ristsumma(nimenumbrid[0] + nimenumbrid[1]);
        Vastus[3] = Ristsumma(p2ev);
        Vastus[4] = Ristsumma(synnip2ev);
        Vastus[5] = Ristsumma(p2ev + kuu);

        return Vastus;

    }

    public static int Ristsumma(int n) {
        int rist = n % 9;
        if (rist == 0) {
            if (n > 0)
                return 9;
        }
        return rist;
    } // Ristsumma leidja.

    public static int[] PytSys(String s) {

        s = s.toLowerCase();

        String Vokaalid[] = new String[10];
        Vokaalid[1] = "aä";
        Vokaalid[2] = "";
        Vokaalid[3] = "uü";
        Vokaalid[4] = "";
        Vokaalid[5] = "e";
        Vokaalid[6] = "oõö";
        Vokaalid[7] = "";
        Vokaalid[8] = "";
        Vokaalid[9] = "i";

        String Konsonandid[] = new String[10];
        Konsonandid[1] = "js";
        Konsonandid[2] = "bkt";
        Konsonandid[3] = "cl";
        Konsonandid[4] = "dmv";
        Konsonandid[5] = "nw";
        Konsonandid[6] = "fx";
        Konsonandid[7] = "gp";
        Konsonandid[8] = "hqz";
        Konsonandid[9] = "r";

        int Vokaalidenumber = 0;
        int Konsonantidenumber = 0;


        if (s.contains("y")){
            int j = s.indexOf("y");

            for (int l = 1; l < 10; l++) {
                for (int i = 0; i < Vokaalid[l].length(); i++) {

                    if (j == 0 && s.length() == 1) {
                        Konsonandid[7] = Konsonandid[7] + 'y';
                    } else if (j + 1 == s.length()) {
                        if (s.charAt(j - 1) == Vokaalid[l].charAt(i)) {
                            Vokaalid[7] = Vokaalid[7] + 'y';
                        }
                    } else if (j == 0 && s.length() > 1) {
                        if (s.charAt(j + 1) == Vokaalid[l].charAt(i)) {
                            Vokaalid[7] = Vokaalid[7] + 'y';
                        }
                    } else if (s.charAt(j + 1) == Vokaalid[l].charAt(i) || s.charAt(j - 1) == Vokaalid[l].charAt(i)) {
                        Vokaalid[7] = Vokaalid[7] + 'y';
                    } else {
                        Konsonandid[7] = Konsonandid[7] + 'y';
                    }

                }


            }
        }
        s = s.replaceAll("\\s","");

        for (int j = s.length() -1; j >= 0; j--) {
            for (int l = 1; l < 10; l++) {
                for (int i = 0; i < Vokaalid[l].length(); i++) {
                    if (Vokaalid[l].charAt(i) == s.charAt(j)) {
                        Vokaalidenumber = Vokaalidenumber + l;
                    }
                }
            }
        }
        for (int j = s.length() - 1; j >= 0; j--) {
            for (int l = 1; l < 10; l++) {
                for (int i = 0; i < Konsonandid[l].length(); i++) {
                    if (Konsonandid[l].charAt(i) == s.charAt(j)) {
                        Konsonantidenumber = Konsonantidenumber + l;
                    }
                }
            }
        }// Pythagorase numbrisüsteem.

        int[] Vastus = new int[]{Vokaalidenumber, Konsonantidenumber};
        return Vastus;

    }




}