package numeroloogia;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

import static numeroloogia.Numeroloogia.LeiaNumbrid;


public class NumeroloogiaGUI extends Application {


    Numeroloogia numeroloogia = new Numeroloogia();

    @Override
    public void start(Stage primaryStage) throws Exception {

        final Font tiitlifont = Font.loadFont("Pieces_of_Eight.ttf", 120);

        Font.loadFont(
                NumeroloogiaGUI.class.getResource("Pieces_of_Eight.ttf").toExternalForm(),
                10
        );


        primaryStage.setTitle("Numeroloogia");


        BorderPane vastuseleheRaam = new BorderPane();
        Scene vastuseleht = new Scene(vastuseleheRaam, 800, 600);
        final Text yks = new Text();
        final Text kaks = new Text();
        final Text kolm = new Text();
        final Text neli = new Text();
        final Text viis = new Text();
        final Text kuus = new Text();
        yks.setFont(Font.font("Tahoma", FontWeight.NORMAL, 25));
        kaks.setFont(Font.font("Tahoma", FontWeight.NORMAL, 25));
        kolm.setFont(Font.font("Tahoma", FontWeight.NORMAL, 25));
        neli.setFont(Font.font("Tahoma", FontWeight.NORMAL, 25));
        viis.setFont(Font.font("Tahoma", FontWeight.BOLD, 25));
        kuus.setFont(Font.font("Tahoma", FontWeight.NORMAL, 25));
        yks.setFill(Color.WHITE);
        kaks.setFill(Color.WHITE);
        kolm.setFill(Color.WHITE);
        neli.setFill(Color.WHITE);
        viis.setFill(Color.WHITE);
        kuus.setFill(Color.WHITE);


        GridPane vastuseraam = new GridPane();
        vastuseraam.add(yks, 0, 0);
        vastuseraam.add(kaks, 1, 0);
        vastuseraam.add(kolm, 2, 0);
        vastuseraam.add(neli, 3, 0);
        vastuseraam.add(viis, 4, 0);
        vastuseraam.add(kuus, 5, 0);

        vastuseleheRaam.setCenter(vastuseraam);
        vastuseraam.setAlignment(Pos.CENTER);


        Button tagasi = new Button("Tagasi");
        HBox tagasikast = new HBox(10);
        tagasikast.setAlignment(Pos.TOP_LEFT);
        tagasikast.getChildren().add(tagasi);
        vastuseleheRaam.setTop(tagasikast);
        tagasikast.setPadding(new Insets(10, 10, 10, 10));

        vastuseleht.getStylesheets().addAll(this.getClass().getResource("resources/style.css").toExternalForm());






        GridPane mainGrid = new GridPane();
        BorderPane avaleheRaam = new BorderPane();
        avaleheRaam.setCenter(mainGrid);
        mainGrid.setPrefSize(300, 300);
        Scene scene = new Scene(avaleheRaam, 800, 600);
        mainGrid.setAlignment(Pos.CENTER);
        primaryStage.setScene(scene);
        scene.getStylesheets().addAll(this.getClass().getResource("resources/style.css").toExternalForm());


        DropShadow vari = new DropShadow();
        vari.setRadius(20.0);
        vari.setColor(Color.BLACK);

        Text tiitel = new Text("Numeroloogia");
        Text alatiitel = new Text("Avasta, mida on elul sulle varuks, numeroloogia väe abil!");
        tiitel.getStyleClass().add("tiitel");
        VBox tiitlikast = new VBox();
        tiitlikast.getChildren().addAll(tiitel,alatiitel);
        tiitlikast.setAlignment(Pos.CENTER);
        tiitlikast.setSpacing(10);
        avaleheRaam.setTop(tiitlikast);
        mainGrid.setVgap(10);
        tiitel.setFill(Color.WHITE);
        alatiitel.setFill(Color.WHITE);
        alatiitel.setFont(Font.font("Tahoma", FontWeight.NORMAL, 16));
        alatiitel.setEffect(vari);




        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);

        Text kasutajanimi = new Text("Nimi:");
        Text synnip2ev = new Text("Sünnipäev:");
        kasutajanimi.setFill(Color.WHITE);
        synnip2ev.setFill(Color.WHITE);


        grid.add(kasutajanimi, 0, 1);
        grid.add(synnip2ev, 0, 2);

        TextField nimev2li = new TextField();


        Integer[] kuup2ev = new Integer[31];
        int j=1;
        for(int i=0;i<31;i++){
            kuup2ev[i]= j;
            j++;
        }
        ObservableList<Integer> p2evad = FXCollections.observableArrayList();
        p2evad.addAll(kuup2ev);
        ComboBox<Integer> p2evav2li = new ComboBox<>(p2evad);


        List<String> kuud = new ArrayList<>(Arrays.asList("jaanuar","veebruar","märts","aprill","mai","juuni","juuli","august","september","oktoober","november","detsember"));
        ComboBox<String> kuuv2li = new ComboBox<String>();
        kuuv2li.getItems().addAll(kuud);


        int praeguneAasta = 2016;
        Integer[] synniAasta = new Integer[praeguneAasta];
        int inc=1;
        for(int i=1;i<praeguneAasta;i++){
            synniAasta[i]= inc;
            inc++;
        }
        ObservableList<Integer> aastad = FXCollections.observableArrayList();
        aastad.addAll(synniAasta);
        Collections.reverse(aastad);
        ComboBox<Integer> aastav2li = new ComboBox<>(aastad);


        HBox synnip2evasisend = new HBox(15);
        GridPane kolmKasti = new GridPane();
        kolmKasti.add(p2evav2li, 1, 0);
        kolmKasti.add(kuuv2li, 2, 0);
        kolmKasti.add(aastav2li, 3, 0);
        synnip2evasisend.getChildren().addAll(kolmKasti);

        grid.add(nimev2li, 1, 1);
        grid.add(synnip2evasisend, 1,2);


        grid.setPadding(new Insets(10, 10, 10, 10));
        grid.setVgap(5);
        grid.setHgap(5);
        mainGrid.add(grid, 0,2 );


        Button nupp = new Button("Leia numbrid");
        HBox nupukast = new HBox(10);
        nupukast.setAlignment(Pos.BOTTOM_CENTER);
        nupukast.getChildren().add(nupp);
        nupukast.setSpacing(10);
        mainGrid.add(nupukast, 0,3 );


        primaryStage.show();







        nupp.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent e) {

                if (!(Pattern.matches("^[a-üA-Ü ]+$", nimev2li.getText()))) {
                    JOptionPane.showMessageDialog(null, "Palun siseta korrektne nimi.", "Viga", JOptionPane.ERROR_MESSAGE);
                }
                else if (p2evav2li.getValue() == null){
                    JOptionPane.showMessageDialog(null, "Palun vali päev.", "Viga", JOptionPane.ERROR_MESSAGE);
                }
                else if (kuuv2li.getValue() == null){
                    JOptionPane.showMessageDialog(null, "Palun vali kuu.", "Viga", JOptionPane.ERROR_MESSAGE);
                }
                else if (aastav2li.getValue() == null){
                    JOptionPane.showMessageDialog(null, "Palun vali aasta.", "Viga", JOptionPane.ERROR_MESSAGE);
                }
                else {
                    primaryStage.setScene(vastuseleht);
                }

                int p2ev = Integer.parseInt(p2evav2li.getSelectionModel().getSelectedItem().toString());
                int kuu = kuuNumbrina(kuuv2li.getSelectionModel().getSelectedItem());
                int aasta = Integer.parseInt(aastav2li.getSelectionModel().getSelectedItem().toString());
                String nimi = nimev2li.getText();



                    int vastus[] = LeiaNumbrid(nimi, p2ev, kuu, aasta);
                    int vastus1 = vastus[0];
                    int vastus2 = vastus[1];
                    int vastus3 = vastus[2];
                    int vastus4 = vastus[3];
                    int vastus5 = vastus[4];
                    int vastus6 = vastus[5];

                    yks.setText(new Integer(vastus1).toString());
                    kaks.setText(new Integer(vastus2).toString());
                    kolm.setText(new Integer(vastus3).toString());
                    neli.setText(new Integer(vastus4).toString());
                    viis.setText(new Integer(vastus5).toString());
                    kuus.setText(new Integer(vastus6).toString());






                }
        });

        nupp.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER)  {

                    if (!(Pattern.matches("^[a-üA-Ü ]+$", nimev2li.getText()))) {
                        JOptionPane.showMessageDialog(null, "Palun siseta korrektne nimi.", "Viga", JOptionPane.ERROR_MESSAGE);
                    }
                    else if (p2evav2li.getValue() == null){
                        JOptionPane.showMessageDialog(null, "Palun vali päev.", "Viga", JOptionPane.ERROR_MESSAGE);
                    }
                    else if (kuuv2li.getValue() == null){
                        JOptionPane.showMessageDialog(null, "Palun vali kuu.", "Viga", JOptionPane.ERROR_MESSAGE);
                    }
                    else if (aastav2li.getValue() == null){
                        JOptionPane.showMessageDialog(null, "Palun vali aasta.", "Viga", JOptionPane.ERROR_MESSAGE);
                    }
                    else {
                        primaryStage.setScene(vastuseleht);
                    }


                        int p2ev = Integer.parseInt(p2evav2li.getSelectionModel().getSelectedItem().toString());
                        int kuu = kuuNumbrina(kuuv2li.getSelectionModel().getSelectedItem());
                        int aasta = Integer.parseInt(aastav2li.getSelectionModel().getSelectedItem().toString());
                        String nimi = nimev2li.getText();



                      int vastus[] = LeiaNumbrid(nimi, p2ev, kuu, aasta);
                       int vastus1 = vastus[0];
                        int vastus2 = vastus[1];
                        int vastus3 = vastus[2];
                        int vastus4 = vastus[3];
                        int vastus5 = vastus[4];
                        int vastus6 = vastus[5];

                        yks.setText(new Integer(vastus1).toString());
                        kaks.setText(new Integer(vastus2).toString());
                        kolm.setText(new Integer(vastus3).toString());
                        neli.setText(new Integer(vastus4).toString());
                        viis.setText(new Integer(vastus5).toString());
                        kuus.setText(new Integer(vastus6).toString());


                    boolean p2evav2liT2idetud = p2evav2li.getSelectionModel().isEmpty();
                    boolean kuuv2liT2idetud = kuuv2li.getSelectionModel().isEmpty();
                    boolean aastav2liT2idetud = aastav2li.getSelectionModel().isEmpty();




                }
            }
        });


        tagasi.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                primaryStage.setScene(scene);
            }
        });

        tagasi.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER)  {

                    primaryStage.setScene(scene);
                }
            }
        });





    }



    public static int kuuNumbrina(String kuu) {
        String[] kuud = {"jaanuar","veebruar","märts","aprill","mai","juuni","juuli","august","september","oktoober","november","detsember"
        };

        int j = 1;
        for (int i = 0; i < 12; i++) {
            if (kuud[i].equals(kuu)) {
                j = j + i;
            }
        }
        return j;
    }




    public static void main(String[] args) {
        launch(args);
    }

}
