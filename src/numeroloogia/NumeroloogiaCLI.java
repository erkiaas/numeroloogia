package numeroloogia;

import java.util.Scanner;

import static numeroloogia.Numeroloogia.LeiaNumbrid;

/**
 * G.McCants'i numeroloogia arvude kalkulaator. CLI versioon.
 * @author eaas
 * @version 0.1
 *
 */
public class NumeroloogiaCLI {
    public static void main(String[] args) {
        //Küsime kasutajalt sünnipäeva.
        Scanner sc = new Scanner(System.in);
        int p2ev;
        int kuu;
        int aasta;

        do {
            System.out.print("Siseta päev: ");
            while (!sc.hasNextInt()) {
                System.out.println("See pole number!");
                sc.next();
            }
            p2ev = sc.nextInt();
        } while (p2ev <= 0);

        do {
            System.out.print("Sisesta kuu: ");
            while (!sc.hasNextInt()) {
                System.out.println("See pole number!");
                sc.next();
            }
            kuu = sc.nextInt();
        } while (kuu <= 0);

        do {
            System.out.print("Sisesta aasta: ");
            while (!sc.hasNextInt()) {
                System.out.println("See pole number!");
                sc.next();
            }
            aasta = sc.nextInt();
        } while (aasta <= 0);

        //Küsime kasutajalt nime.
        Scanner scanner = new Scanner(System.in);
        System.out.println("Sisesta nimi: ");
        String nimi = scanner.nextLine();


        int vastus[] = LeiaNumbrid(nimi, p2ev, kuu, aasta);

        System.out.println("Sinu hinge number on: " + vastus[0] );
        System.out.println("Sinu isiksuse number on: " + vastus[1]);
        System.out.println("Sinu nime väe number on: " + vastus[2]);
        System.out.println("Sinu sünnipäeva number on: " + vastus[3]);
        System.out.println("Sinu elutee number on: " + vastus[4]);
        System.out.println("Sinu hoiaku number on: " + vastus[5]);
    }
}
